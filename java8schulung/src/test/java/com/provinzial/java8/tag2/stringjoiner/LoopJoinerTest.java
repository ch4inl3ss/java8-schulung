package com.provinzial.java8.tag2.stringjoiner;
import static org.hamcrest.CoreMatchers.*;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.*;

public class LoopJoinerTest {


    private LoopJoiner loopJoiner = new LoopJoiner();

    @Test
    public void testJoinString(){
        assertThat(loopJoiner.joinString(), is(equalTo("Namen: [ Name: Dade Murphy," +
                " Name: Kate Libby, Name: Joey Pardella, Name: Emmanuel Goldstein, Name: Paul Cook]")));
    }

    @Test
    public void testStringNotJoinerUsed() throws Exception{
        List<String> strings = FileUtils.readLines(new File("./src/main/java/com/provinzial/java8/tag2/stringjoiner/LoopJoiner.java"), Charset.defaultCharset());

        assertThat(strings.stream().anyMatch(s -> s.contains("import java.util.StringJoiner;")), is(equalTo(false)));
    }
}