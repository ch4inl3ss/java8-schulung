package com.provinzial.java8.tag2.money;

import com.provinzial.java8.tag2.money.CurrencyExample;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Currency;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyExampleTest {

    private CurrencyExample currencyExample = new CurrencyExample();

    private Currency currency = Currency.getInstance(Locale.GERMANY);


    @Test
    public void sollWaehrungseinheitLiefern(){
        String result = currencyExample.gibWaehrungsEinheitFuerDeutschlandZurueck(currency);
        assertThat(result, is(equalTo("EUR")));
    }


    @Test
    public void sollWaehrungszeichenLiefern(){
        String result = currencyExample.gibWaehrungsZeichenZurueck(currency);
        assertThat(result, is(equalTo("€")));
    }




    @Test
    public void sollWaehrungsZeichenDollarZurueckLiefern(){
        String result = currencyExample.gibWaehrungsZeichenUSAZurueck();
        assertThat(result, is(equalTo("$")));
    }
}