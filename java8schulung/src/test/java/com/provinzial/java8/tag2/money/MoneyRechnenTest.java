package com.provinzial.java8.tag2.money;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MoneyRechnenTest {

    private MoneyRechnen moneyRechnen = new MoneyRechnen();

    @Test
    public void sollAufZweiNachkommastellenRunden(){

        double v = 1.30573908;

        String mitZweiNachkommastellen = moneyRechnen.liefereBetragMitZweiNachkommastellen(v);

        assertThat(mitZweiNachkommastellen, is(equalTo("EUR 1.31")));

    }
}