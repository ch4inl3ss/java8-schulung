package com.provinzial.java8.tag2.optional;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CustomerFinderWithOptionalTest {

    private CustomerFinderWithOptional customerFinderWithOptional = new
            CustomerFinderWithOptional();

    @Test
    public void sollCustomerFinden(){
        Stream<Customer> customers = customerFinderWithOptional.findCustomers(Arrays.asList("1", "3", "4"));
        List<Customer> customerList = customers.collect(Collectors.toList());
        assertThat(customerList.size(), is(equalTo(2)));
        assertThat(customerList.get(0).getName(), is(equalTo("Otto")));
        assertThat(customerList.get(1).getName(), is(equalTo("Henri")));
    }

}