package com.provinzial.java8.tag2.money;

import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CurrencyUnitExampleTest {

    private  CurrencyUnitExample currencyUnitExample = new CurrencyUnitExample();

    @Test
    public void sollEURZurueckGeben(){
        String waehrungsEinheitFuerDeutschland = currencyUnitExample.gibWaehrungsEinheitFuerDeutschlandZurueck();
        assertThat(waehrungsEinheitFuerDeutschland, is(equalTo("EUR")));
    }


    @Test
    public void sollMonetaryAmountBenutzen() throws Exception {
        List<String> strings = FileUtils.readLines(new File("./src/main/java/com/provinzial/java8/tag2/money/CurrencyUnitExample.java"), Charset.defaultCharset());

        assertThat(strings.stream().anyMatch( s-> s.contains("import javax.money")), is(equalTo(true)));
    }
}