package com.provinzial.java8.tag2.decoratorpattern.old;

import com.provinzial.java8.tag2.optional.Customer;
import com.provinzial.java8.tag2.strategypattern.PrintStrategy;
import com.provinzial.java8.tag2.strategypattern.old.ShortPrefixPrintStrategy;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class YourDecoratorTest {

    @Test
    public void sollVonTextDecoratorErben() throws Exception{


        BasicText basicText = new BasicText("YourDecorator ");
        Object[] params = {basicText};

        assertThat(YourDecorator.class.getSuperclass(), is(equalTo(TextDecorator.class)));

        Method gibText = YourDecorator.class.getMethod("gibText",null);

        Constructor constructor = YourDecorator.class.getConstructor(new Class[]{Text.class});
        YourDecorator decorator= (YourDecorator)constructor.newInstance(params);

        String printedString = (String) gibText.invoke(decorator);
        assertThat(printedString, is(equalTo("YourDecorator funktioniert")));

    }
}