package com.provinzial.java8.tag2.strategypattern.old;

import com.provinzial.java8.tag2.optional.Customer;
import com.provinzial.java8.tag2.strategypattern.PrintStrategy;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ShortPrefixPrintStrategyTest {

    @Test
    public void sollPrintStrategyImplementieren() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

        Customer phil = new Customer("10", "Phil", 40);

        assertThat(ShortPrefixPrintStrategy.class.getInterfaces().length, is(equalTo(1)));
        assertThat(ShortPrefixPrintStrategy.class.getInterfaces()[0], is(equalTo(PrintStrategy.class)));

        //hole print()-Methode
        Method print = ShortPrefixPrintStrategy.class.getMethod("print", Customer.class);
        String printedPhil = (String) print.invoke(ShortPrefixPrintStrategy.class.newInstance(), phil);
        assertThat(printedPhil, is(equalTo("Customer[id=10,name=Phil,age=40]")));
    }



}