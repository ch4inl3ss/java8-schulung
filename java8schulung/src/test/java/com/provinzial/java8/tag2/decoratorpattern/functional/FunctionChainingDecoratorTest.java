package com.provinzial.java8.tag2.decoratorpattern.functional;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Method;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FunctionChainingDecoratorTest {



    @Test
    public void sollFunctionMethodeHaben() throws Exception{

        Function function = FunctionChainingDecorator.yourFunction();
        assertThat(function.apply("Test"), is(equalTo("TestYourFunction")));
    }

    @Test
    public void sollChainingMethodeHaben(){
        Function<String, String> mockFunc = Mockito.mock(Function.class);
        Mockito.when(mockFunc.apply("funktioniert")).thenReturn("");
        Mockito.when(mockFunc.andThen(FunctionChainingDecorator.yourFunction())).thenReturn(FunctionChainingDecorator.yourFunction());
        String chained = FunctionChainingDecorator.chained(mockFunc);
        Mockito.verify(mockFunc, Mockito.times(1)).andThen(Mockito.any(Function.class));
        assertThat(chained, is(equalTo("funktioniertYourFunction")));
    }

}