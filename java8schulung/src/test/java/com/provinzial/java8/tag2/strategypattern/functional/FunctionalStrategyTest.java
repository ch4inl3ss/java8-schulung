package com.provinzial.java8.tag2.strategypattern.functional;

import com.provinzial.java8.tag2.optional.Customer;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FunctionalStrategyTest {

    private Customer customer = new Customer("10", "Phil", 40);


    @Test
    public void sollJsonStrategyImplementieren(){
        String printedPhil = FunctionalStrategy.liefereJsonPrintStrategy().print(customer);
        assertThat(printedPhil, is(equalTo("{\"id\":\"10\",\"name\":\"Phil\",\"age\":40}")));
    }

    @Test
    public void sollShortPrefixStrategyImplementieren(){
        String printetPhil = FunctionalStrategy.liefereShortPrefixPrintStrategy().print(customer);
        assertThat(printetPhil, is(equalTo("Customer[id=10,name=Phil,age=40]")));
    }


}