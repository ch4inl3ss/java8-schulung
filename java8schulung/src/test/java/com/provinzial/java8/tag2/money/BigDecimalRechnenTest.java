package com.provinzial.java8.tag2.money;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BigDecimalRechnenTest {

    private BigDecimalRechnen bigDecimalRechnen = new BigDecimalRechnen();


    @Test
    public void sollNurNochZweiNachkommastellenLiefern(){
        String betragAufZweiNachkommastellenGerundet = bigDecimalRechnen.liefereBetragAufZweiNachkommastellenGerundetZurueck(new BigDecimal("12.125"));
        assertThat(betragAufZweiNachkommastellenGerundet, is(equalTo("12.13")));

    }

}