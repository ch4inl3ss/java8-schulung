package com.provinzial.java8.tag2.datetime.locale;

import org.junit.Before;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LocalExampleTest {

    private LocalExample localDateExample;

    @Before
    public void setUp(){
        localDateExample=new LocalExample();
    }

    @Test
    public void sollDifferenzBerechnen(){
        int i = localDateExample.zeitDifferenz();
        ZonedDateTime nowBerlin = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));
        ZonedDateTime nowLondon = ZonedDateTime.now(ZoneId.of("Europe/London"));
        assertThat(i, is(equalTo(Math.abs(nowBerlin.getHour()-nowLondon.getHour()))));
    }

}