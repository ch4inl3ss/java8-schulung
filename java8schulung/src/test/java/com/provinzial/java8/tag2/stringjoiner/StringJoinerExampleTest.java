package com.provinzial.java8.tag2.stringjoiner;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class StringJoinerExampleTest {

    private StringJoinerExample stringJoiner = new StringJoinerExample();

    @Test
    public void testJoinString(){
        assertThat(stringJoiner.joinString(), is(equalTo("Namen: [ Name: Dade Murphy," +
                " Name: Kate Libby, Name: Joey Pardella, Name: Emmanuel Goldstein, Name: Paul Cook]")));
    }

    @Test
    public void testStringJoinerUsed() throws Exception{
        List<String> strings = FileUtils.readLines(new File("./src/main/java/com/provinzial/java8/tag2/stringjoiner/StringJoinerExample.java"), Charset.defaultCharset());
        assertThat(strings.stream().anyMatch(s -> s.contains("import java.util.StringJoiner;")), is(equalTo(true)));
    }
}