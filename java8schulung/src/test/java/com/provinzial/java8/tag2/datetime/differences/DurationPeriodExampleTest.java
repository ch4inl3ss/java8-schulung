package com.provinzial.java8.tag2.datetime.differences;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DurationPeriodExampleTest {

    private DurationPeriodExample durationPeriodExample=new DurationPeriodExample();

    @Test
    public void sollPeriodLiefern(){
        long zeitDifferenzInTagen = durationPeriodExample.zeitDifferenzInTagen(LocalDate.now(), LocalDate.now().plusDays(1337));
        assertThat(zeitDifferenzInTagen, is(equalTo(1337l)));
    }

    @Test
    public void sollDurtationLiefern(){
        long zeitDifferenzInSekunden = durationPeriodExample.zeitDifferenzInSekunden(LocalTime.now(), LocalTime.now().plusSeconds(1337));
        assertThat(zeitDifferenzInSekunden, is(equalTo(1337l)));

    }


}