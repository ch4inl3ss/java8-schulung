package com.provinzial.java8.tag2.datetime.calculate;

import com.provinzial.java8.tag2.datetime.calculate.DateHandler;
import com.provinzial.java8.tag2.datetime.calculate.JavaUtilDate;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by ch4inl3ss on 26.06.17.
 */
public class JavaUtilDateTest {

    private DateHandler dateHandler;

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");


    @Before
    public void setUp(){
        dateHandler=new JavaUtilDate();
    }

    @Test
    public void shouldOnlyUseJavaUtilDate() throws Exception{

        List<String> strings = FileUtils.readLines(new File("./src/main/java/com/provinzial/java8/tag2/datetime/calculate/JavaUtilDate.java"), Charset.defaultCharset());
        assertThat(strings.stream().anyMatch(s -> s.contains("import java.time")), is(equalTo(false)));

    }

    @Test
    public void getToday() throws Exception {
        String date = LocalDate.now().format(dtf);
        assertThat(dateHandler.getToday(), is(equalTo(date)));
    }

    @Test
    public void getTommorow() throws Exception {
        String date = LocalDate.now().plusDays(1).format(dtf);
        assertThat(dateHandler.getTommorow(), is(equalTo(date)));
    }

    @Test
    public void getNextWeek() throws Exception {
        String date = LocalDate.now().plusWeeks(1).format(dtf);
        assertThat(dateHandler.getNextWeek(), is(equalTo(date)));
    }

    @Test
    public void getNextYear() throws Exception {
        String date = LocalDate.now().plusYears(1).format(dtf);
        assertThat(dateHandler.getNextYear(), is(equalTo(date)));
    }

}