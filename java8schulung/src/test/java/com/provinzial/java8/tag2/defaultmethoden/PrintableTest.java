package com.provinzial.java8.tag2.defaultmethoden;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PrintableTest {

    Customer customer = new Customer();

    @Test
    public void sollPrinten() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        assertThat(Customer.class.getInterfaces().length, is(equalTo(1)));
        Method print = Customer.class.getMethod("print", null);

        assertThat(print, is(not(nullValue())));

        Object invoke = print.invoke(customer);
        String result = (String) invoke;

        assertThat(result, is(equalTo("{\"name\":\"Dade Murphy\",\"age\":\"18\"}")));
    }
}