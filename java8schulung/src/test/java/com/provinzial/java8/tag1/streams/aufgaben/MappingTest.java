package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class MappingTest {

    @Spy
    private List<String> lowerCaseStrings = new ArrayList<String>();

    @Spy
    private List<String> namensListe = new ArrayList<String>();

    @Before
    public void setUp() throws Exception {
        lowerCaseStrings.add("ich");
        lowerCaseStrings.add("bin");
        lowerCaseStrings.add("komplett");
        lowerCaseStrings.add("in");
        lowerCaseStrings.add("lowercase");
        lowerCaseStrings.add("geschrieben");
        namensListe.add("Bernhard");
        namensListe.add("Brian");
        namensListe.add("Samson der sarazenische Ochse");
        namensListe.add("Stechus Kaktus");
    }

    @Test
    public final void testToUpperCase() throws Exception {
        List<String> upperCaseListe = Mapping.toUpperCase(lowerCaseStrings);
        assertThat("Die UpperCaseListe ist leer!", upperCaseListe.isEmpty(), is(false));
        verify(lowerCaseStrings, times(1)).stream();
        upperCaseListe.forEach(s -> assertTrue(StringUtils.isAllUpperCase(s)));
    }

    @Test
    public void testToPersonal() throws Exception {
        List<Personal> personalListe = Mapping.toPersonal(namensListe);
        assertThat("Die UpperCaseListe ist leer!", personalListe.isEmpty(), is(false));
        verify(namensListe, times(1)).stream();
        assertThat(namensListe.size(), is(equalTo(personalListe.size())));
        personalListe.forEach(n -> assertThat(namensListe.contains(n.getName()), is(true)));
    }

}
