package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SumReduceTest {

    private static final int SUMME = 8094;

    @Spy
    private List<Integer> werteListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        werteListe.add(5);
        werteListe.add(25);
        werteListe.add(2500);
        werteListe.add(3000);
        werteListe.add(2499);
        werteListe.add(65);
    }

    @Test
    public void testGetSum() throws Exception {
        int sum = SumReduce.getSum(werteListe);
        verify(werteListe, times(1)).stream();
        assertThat("Summe ist nicht korrekt", sum, is(SUMME));
    }

    @Test
    public void testGetSumPerReduce() throws Exception {
        int sum = SumReduce.getSumPerReduce(werteListe);
        verify(werteListe, times(1)).stream();
        assertThat("Summe ist nicht korrekt", sum, is(SUMME));
    }

}
