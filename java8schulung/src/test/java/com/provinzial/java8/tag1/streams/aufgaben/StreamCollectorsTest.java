package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class StreamCollectorsTest {

    @Spy
    private List<Personal> personalListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        personalListe.add(new Personal("Agusta", 18));
        personalListe.add(new Personal("Cooper", 15));
        personalListe.add(new Personal("Auster", 26));
        personalListe.add(new Personal("Augusto", 55));
        personalListe.add(new Personal("Knappe", 50));
        personalListe.add(new Personal("Müller", 26));
        personalListe.add(new Personal("Zacharias", 50));
        personalListe.add(new Personal("Klein", 16));
        personalListe.add(new Personal("Altmeier", 18));
    }

    @Test
    public final void testPartitioning() throws Exception {
        Map<Boolean, List<Personal>> partitioning = StreamCollectors.partitioning(personalListe);
        verify(personalListe).stream();
        List<Personal> volljaehrigesPersonal = partitioning.get(Boolean.TRUE);
        List<Personal> minderjaehrigesPersonal = partitioning.get(Boolean.FALSE);
        volljaehrigesPersonal.forEach(p -> assertThat(p.getAlter() >= 18, is(true)));
        minderjaehrigesPersonal.forEach(p -> assertThat(p.getAlter() < 18, is(true)));
    }

    @Test
    public final void testGrouping() throws Exception {
        Map<Integer, List<Personal>> grouping = StreamCollectors.grouping(personalListe);
        verify(personalListe).stream();
        Set<Integer> altersGruppen = grouping.keySet();
        List<Personal> personal = grouping.values().stream().flatMap(List::stream).collect(Collectors.toList());
        assertThat(CollectionUtils.isEqualCollection(personalListe, personal), is(true));
        altersGruppen.forEach(i -> {
            grouping.get(i).stream().forEach(p -> assertThat(p.getAlter(), is(equalTo(i))));
        });
    }

    @Test
    public final void testSum() throws Exception {
        int sum = StreamCollectors.sum(personalListe);
        int vergleichsSumme = berechneAlterssumme();
        verify(personalListe).stream();
        assertThat(sum, is(vergleichsSumme));
    }

    @Test
    public void testConcatenating() throws Exception {
        String concatenating = StreamCollectors.concatenating(personalListe);
        assertThat(concatenating, containsString(","));
        assertThat(concatenating.endsWith("."), is(true));
        String namesWithoutPoint = concatenating.replace(".", StringUtils.EMPTY);
        String[] namen = namesWithoutPoint.split(",");
        List<String> namenListe = Arrays.asList(namen);
        List<String> ursprungsNamen =
            personalListe.stream().map(Personal::getName).map(a -> a.trim()).collect(Collectors.toList());
        assertThat(CollectionUtils.containsAll(namenListe, ursprungsNamen), is(true));
    }

    private int berechneAlterssumme() {
        int vergleichsSumme = 0;
        for (Personal personal : personalListe) {
            vergleichsSumme += personal.getAlter();
        }
        return vergleichsSumme;
    }

}
