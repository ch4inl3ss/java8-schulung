package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.provinzial.java8.tag1.streams.aufgaben.FlatMapper;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("javadoc")
public class FlatMapperTest {

    @Spy
    private List<List<String>> gruppenNamen = new ArrayList<>();

    private List<String> gruppe1;

    private List<String> gruppe2;

    private List<String> gruppe3;

    @Before
    public void setUp() throws Exception {
        gruppe1 = Arrays.asList("Theodor", "Andreas", "VonUndZuBergischUndSo");
        gruppe2 = Arrays.asList("Markus", "Maria", "SagIchNicht");
        gruppe3 = Arrays.asList("Sebastian", "Felix", "Bastian");
        gruppenNamen.addAll(Arrays.asList(gruppe1, gruppe2, gruppe3));
    }

    @Test
    public void testTransformToFlatMap() throws Exception {
        List<String> namensListe = FlatMapper.transformToFlatMap(gruppenNamen);
        assertThat("Die Namensliste ist leer!", namensListe.isEmpty(), is(false));
        verify(gruppenNamen, times(1)).stream();
        assertThat(namensListe.containsAll(gruppe1), is(true));
        assertThat(namensListe.containsAll(gruppe2), is(true));
        assertThat(namensListe.containsAll(gruppe3), is(true));
    }

}
