package com.provinzial.java8.tag1.streams.aufgaben;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class Java7ToJava8Test {

    @Spy
    private List<Double> preisListe = new ArrayList<>();

    @Spy
    private List<Steuer> steuerListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        preisListe.add(200.0);
        preisListe.add(500.0);
        preisListe.add(1000.0);
        preisListe.add(100.0);
        preisListe.add(20.0);
        steuerListe.add(Steuer.MWST19);
        steuerListe.add(Steuer.MWST7);
        steuerListe.add(Steuer.MWST19);
        steuerListe.add(Steuer.MWST7);
        steuerListe.add(Steuer.MWST7);
    }

    @Test
    public void testGetTotalCostsInJava8() throws Exception {
        double totalCostsJava7 = Java7ToJava8.getTotalCosts(preisListe, steuerListe);
        double totalCostsInJava8 = Java7ToJava8.getTotalCostsInJava8(preisListe, steuerListe);
        assertEquals(totalCostsJava7, totalCostsInJava8, 0);
    }

}
