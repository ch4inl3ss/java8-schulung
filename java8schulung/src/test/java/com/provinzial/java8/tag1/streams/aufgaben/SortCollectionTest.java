/**
 *
 */
package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author PD05843
 */
@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class SortCollectionTest {

    @Spy
    private List<Personal> personalListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        personalListe.add(new Personal("Müller", 26));
        personalListe.add(new Personal("Zacharias", 45));
        personalListe.add(new Personal("Altmeier", 24));
        personalListe.add(new Personal("Auster", 26));
        personalListe.add(new Personal("Agusta", 18));
        personalListe.add(new Personal("Knappe", 50));
        personalListe.add(new Personal("Augusto", 55));
    }

    /**
     * Test method for {@link com.provinzial.java8.tag1.streams.aufgaben.SortCollection#sortPersonal(java.util.List)}.
     */
    @Test
    public final void testSortPersonal() throws Exception {
        List<String> sortierteNamen = SortCollection.sortPersonal(personalListe);
        assertThat("Die Liste ist leer!", sortierteNamen.isEmpty(), is(false));
        verify(personalListe).stream();
        List<String> sortierteUrsprungsNamen = sortiereNamen();
        IntStream.range(0, sortierteNamen.size())
            .forEach(i -> assertThat(sortierteNamen.get(i), is(equalTo(sortierteUrsprungsNamen.get(i)))));
    }

    private List<String> sortiereNamen() {
        List<String> namen = new ArrayList<>();
        for (Personal personal : personalListe) {
            namen.add(personal.getName());
        }
        Collections.sort(namen);
        return namen;
    }

}
