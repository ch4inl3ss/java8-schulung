package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class IntSummaryTest {

    @Spy
    private List<Personal> personalListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        personalListe.add(new Personal("Agusta", 18));
        personalListe.add(new Personal("Altmeier", 24));
        personalListe.add(new Personal("Auster", 26));
        personalListe.add(new Personal("Augusto", 55));
        personalListe.add(new Personal("Müller", 26));
        personalListe.add(new Personal("Zacharias", 45));
        personalListe.add(new Personal("Knappe", 50));
    }

    @Test
    public void testCreateIntSummary() throws Exception {
        IntSummaryStatistics intSummary = IntSummary.createIntSummary(personalListe);
        double average = ermittleDurchschnitt();
        long summe = ermittleSumme();
        verify(personalListe).stream();
        assertThat(intSummary.getMax(), is(55));
        assertThat(intSummary.getMin(), is(18));
        assertThat(intSummary.getSum(), is(summe));
        assertThat(intSummary.getCount(), is((long) personalListe.size()));
        assertThat(intSummary.getAverage(), is(average));
    }

    private long ermittleSumme() {
        long sum = 0;
        for (Personal personal : personalListe) {
            sum += personal.getAlter();
        }
        return sum;
    }

    private double ermittleDurchschnitt() {
        long summe = ermittleSumme();
        return (double) summe / personalListe.size();
    }

}
