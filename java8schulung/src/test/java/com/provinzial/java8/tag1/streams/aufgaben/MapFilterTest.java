package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class MapFilterTest {

    @Spy
    private List<Personal> personalListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        personalListe.add(new Personal("Agusta", 18));
        personalListe.add(new Personal("Altmeier", 24));
        personalListe.add(new Personal("Auster", 26));
        personalListe.add(new Personal("Augusto", 55));
        personalListe.add(new Personal("Müller", 26));
        personalListe.add(new Personal("Zacharias", 45));
        personalListe.add(new Personal("Knappe", 50));
    }

    @Test
    public void testFilterPersonal() throws Exception {
        List<String> namen = MapFilter.filterPersonal(personalListe);
        assertThat("Die UpperCaseListe ist leer!", namen.isEmpty(), is(false));
        verify(personalListe, times(1)).stream();
        assertThat(namen, hasItems("Auster", "Augusto"));
    }

}
