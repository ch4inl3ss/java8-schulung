package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.provinzial.java8.tag1.streams.aufgaben.FilterCollection;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class FilterCollectionTest {

    private static final int LAENGE = 3;

    @Spy
    private List<String> strings = new ArrayList<String>();

    @Before
    public void setUp() throws Exception {
        strings.add("ich");
        strings.add("habe");
        strings.add("strings");
        strings.add("verschiedener");
        strings.add("laenge");
    }

    @Test
    public final void testFilterSpecificLength() throws Exception {
        List<String> gefilterteStrings = FilterCollection.filterSpecificLength(strings, LAENGE);
        assertThat("Die gefilterte Liste ist leer!", gefilterteStrings.isEmpty(), is(false));
        verify(strings, times(1)).stream();
        gefilterteStrings.forEach(s -> assertTrue(s.length() <= LAENGE));
    }

}
