package com.provinzial.java8.tag1.streams.aufgaben;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.provinzial.java8.tag1.streams.aufgaben.MinMaxComparator;

@RunWith(MockitoJUnitRunner.class)
public class MinMaxComparatorTest {

    private static final int MIN_WERT = 3;

    private static final int MAX_WERT = 3000;

    @Spy
    private List<Integer> werteListe = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        werteListe.add(5);
        werteListe.add(25);
        werteListe.add(2500);
        werteListe.add(MAX_WERT);
        werteListe.add(2499);
        werteListe.add(65);
        werteListe.add(MIN_WERT);
    }

    @Test
    public void testGetMaxEntry() throws Exception {
        int maxWert = MinMaxComparator.getMaxEntry(werteListe);
        verify(werteListe, times(1)).stream();
        assertThat("Der Maximal-Wert ist nicht korrekt", maxWert, is(MAX_WERT));
    }

    @Test
    public void testGetMinEntry() throws Exception {
        int minWert = MinMaxComparator.getMinEntry(werteListe);
        verify(werteListe, times(1)).stream();
        assertThat("Der Minimal-Wert ist nicht korrekt", minWert, is(MIN_WERT));
    }

}
