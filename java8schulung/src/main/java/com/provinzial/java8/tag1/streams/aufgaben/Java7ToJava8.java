package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Java7ToJava8 {

    public static double getTotalCosts(final List<Double> preisListe, final List<Steuer> steuerListe) {
        List<Kosten> kostenListe = new ArrayList<>();
        double gesamtKosten = 0;
        for (int i = 0; i < preisListe.size(); i++) {
            Kosten kosten = new Kosten(steuerListe.get(i), preisListe.get(i));
            kostenListe.add(kosten);
        }
        for (Kosten kosten : kostenListe) {
            gesamtKosten += kosten.berechneBrutto();
        }
        return gesamtKosten;

    }

    public static double getTotalCostsInJava8(final List<Double> preisListe, final List<Steuer> steuerListe) {
        // @formatter:off
        //TODO: Aufgabe 0: Optionale Stream-Aufgabe. Konvertiere den oben geschriebenen Quellcode in Java8-Streams
        //Benutze dabei die Kosten Klasse um den Brutto zu berechnen
        return 0;
        // @formatter:on
    }

    public static class Kosten {

        private Steuer steuer;

        private double kosten;

        /**
         * @param steuer
         * @param kosten
         */
        public Kosten(Steuer steuer, double kosten) {
            super();
            this.steuer = steuer;
            this.kosten = kosten;
        }

        public double berechneBrutto() {
            return kosten + kosten * (steuer.getProzentsatz() / 100.0);
        }

    }

}
