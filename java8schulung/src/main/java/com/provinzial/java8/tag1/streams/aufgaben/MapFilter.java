package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;

public class MapFilter {

    /**
     * Sucht alle Angestellten, deren Name mit "A" beginnt und die aelter als 25 Jahre sind.
     * @param personalList
     * @return Liste mit Namen der Angestellten
     */
    public static List<String> filterPersonal(List<Personal> personalList) {
        // TODO Aufgabe 7: Suche alle Angestellten die aelter als 25 Jahre alt sind und deren Namen mit A beginnen.
        // Gebe alle Namen der Personen aus.
        // Führe danach den JUnit-Testfall aus, um das Ergebnis zu testen.
        return Collections.emptyList();
    }

}
