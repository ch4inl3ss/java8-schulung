package com.provinzial.java8.tag1.lambda.methodenreferenzen;

public class TestClass {
    private int field;

    public TestClass(int field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "TestClass{" +
                "field=" + field +
                '}';
    }
}