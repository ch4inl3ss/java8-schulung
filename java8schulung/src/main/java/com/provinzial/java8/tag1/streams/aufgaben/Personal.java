package com.provinzial.java8.tag1.streams.aufgaben;

public class Personal {
    private String name;

    private int alter;

    /**
     * Konstruktor
     * @param name
     * @param alter
     */
    public Personal(String name, int alter) {
        super();
        this.name = name;
        this.alter = alter;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the alter
     */
    public int getAlter() {
        return alter;
    }

    /**
     * @param alter the alter to set
     */
    public void setAlter(int alter) {
        this.alter = alter;
    }

}