package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;

/**
 * @author PD05843
 */
public class FlatMapper {

    /**
     * Erzeugt aus einer Liste von Listen eine FlatMap
     * @param liste
     * @return
     */
    public static List<String> transformToFlatMap(List<List<String>> liste) {
        // TODO Aufgabe 4: Verarbeite alle Listen von Listen und erzeuge eine eindimensionale Liste mit den Strings der
        // uebergebenen Listen.
        // Der Testfall sollte nach korrekter Implementierung gruen sein.
        return Collections.emptyList();
    }

}
