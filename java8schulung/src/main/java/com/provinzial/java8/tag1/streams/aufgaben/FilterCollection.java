package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;

public class FilterCollection {

    /**
     * Filtert alle Strings aus einer Liste heraus, die laenger als angegeben sind.
     * @param stringList
     * @param length
     * @return List
     */
    public static List<String> filterSpecificLength(List<String> stringList, final int length) {
        // TODO Aufgabe 2: Es sollen alle Strings aus einer Liste entfernt werden, die eine bestimmte Laenge ueberschreiten.
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return Collections.emptyList();
    }

}
