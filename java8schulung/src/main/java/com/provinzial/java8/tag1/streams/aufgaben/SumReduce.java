package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.List;

public class SumReduce {

    /**
     * Gibt die Summe aller Werte zurueck
     * @param integerList
     * @return int
     */
    public static int getSum(List<Integer> integerList) {
        // TODO Aufgabe 6a: Es soll die Summe aller Werte aus der integerList zurueckgegeben werden.
        // Dafuer soll die integerList in einen IntStream konvertiert und die sum() funktion benutzt werden
        // Der Testfall sollte nach korrekter Implementierung gruen sein.
        return 0;
    }

    /**
     * Gibt die Summe aller Werte zurueck
     * @param integerList
     * @return int
     */
    public static int getSumPerReduce(List<Integer> integerList) {
        // TODO Aufgabe 6b: Es soll die Summe aller Werte aus der integerList zurueckgegeben werden.
        // Dafuer soll die integerList in einen stream konvertiert und die reduce() funktion benutzt werden
        // Der Testfall sollte nach korrekter Implementierung gruen sein.
        return 0;
    }

}
