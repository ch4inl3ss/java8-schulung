package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class StreamCollectors {

    /**
     * Soll alle Personen aufteilen in Erwachsene (>= 18) und Minderjaehrige
     * @param personalListe
     * @return Map
     */
    public static Map<Boolean, List<Personal>> partitioning(List<Personal> personalListe) {
        // TODO Aufgabe 9a: Es sollen alle Personen aelter als 18 Jahre in eine eigene Liste und alle darunter liegenden
        // Personen in eine andere Liste.
        // Benutze Collectors.partitioningBy fuer die Aufteilung
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return Collections.emptyMap();
    }

    /**
     * Soll alle Personen nach Alter sortieren
     * @param personalListe
     * @return Map
     */
    public static Map<Integer, List<Personal>> grouping(List<Personal> personalListe) {
        // TODO Aufgabe 9b: Es sollen alle Personen nach Alter gruppiert werden.
        // Benutze Collectors.groupingBy fuer die Aufteilung
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return Collections.emptyMap();
    }

    /**
     * Soll das Alter aller Personen aufsummieren
     * @param personalListe
     * @return Map
     */
    public static int sum(List<Personal> personalListe) {
        // TODO Aufgabe 9c: Es soll das Alter aller Personen aufsummiert werden.
        // Benutze Collectors.summingInt fuer die Aufsummierung
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return 0;
    }

    /**
     * Soll die Namen aller Angestellten kommasepariert auflisten und mit einem Punkt abschließen
     * @param personalListe
     * @return String
     */
    public static String concatenating(List<Personal> personalListe) {
        // TODO Aufgabe 9d: Es sollen die Namen aller Angestellten kommasepariert aufgelistet und mit einem Punkt abgeschlossen
        // werden. Benutze Collectors.joining fuer die Konkatinierung genutzt werden
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return StringUtils.EMPTY;
    }

}
