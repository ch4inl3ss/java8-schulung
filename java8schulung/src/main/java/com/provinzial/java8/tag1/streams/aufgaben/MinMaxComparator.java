package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.List;

public class MinMaxComparator {

    /**
     * Gibt den hoechsten Eintrag einer Liste von Integern zurück
     * @param integerList
     * @return int
     */
    public static int getMaxEntry(List<Integer> integerList) {
        // TODO Aufgabe 5a: Es soll der hoechste Wert aus einer Liste von Werten herausgesucht werden.
        // Der Testfall sollte nach korrekter Implementierung gruen sein.
        return 0;
    }

    /**
     * Gibt den niedristen Eintrag einer Liste von Integern zurück
     * @param integerList
     * @return int
     */
    public static int getMinEntry(List<Integer> integerList) {
        // TODO Aufgabe 5b: Es soll der niedriste Wert aus einer Liste von Werten herausgesucht werden.
        // Der Testfall sollte nach korrekter Implementierung gruen sein.
        return 0;
    }

}
