package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;

public class SortCollection {

    /**
     * Sortiert alle Namen in der Personalliste nach Alphabet und gibt eine Liste von Namen zurück
     * @param personalListe
     * @return sortierte Namensliste
     */
    public static List<String> sortPersonal(List<Personal> personalListe) {
        // TODO Aufgabe 3: Es sollen alle Namen in eine Liste gesetzt und diese alphabetisch sortiert werden
        // Nach der korrekten Implementierung mit streams, sollte der beigefuegte Testfall gruen durchlaufen
        return Collections.emptyList();
    }

}
