package com.provinzial.java8.tag1.lambda.einleitung;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.CollectionUtils;


public class EinleitungLambdas {


    private static void lambdas() {
        // Iterieren ueber eine Stringliste
        List<String> list = Arrays.asList("hallo", "welt", "23", "42");
        List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5);

        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("___________TRENNLINIE_____________");

        for (Integer i : intList) {
            System.out.println(i + 1);
        }

        System.out.println("___________TRENNLINIE_____________");

        // In Java gibt es keinen Typen fuer Funktionen.
        // Ich moechte also einem Objekt eine bestimmte Funktion uebergeben, die
        // es dann auf sich ausfuehren soll. Das gibt es bisher nicht.
        // Man moechte z.B. einer Liste sagen, dass sie alle ihre Elemente
        // ausgeben soll oder aber auf alle ihre Elemente +1 addieren und
        // ausgeben soll.

        // So einen funktionalen Ausdruck kennt man vielleicht als Closure.
        // Closures gab es z.B. in einer Zusatzbibliothek Guava von Google.

        CollectionUtils.forAllDo(list, new Closure<String>() {

            @Override
            public void execute(String s) {
                assert s != null;
                System.out.println(s);
            }
        });

        System.out.println("___________TRENNLINIE_____________");

        // Damit hatte man sowas erreicht wie einen Funktionstyp (Closure), den
        // man uebergeben konnte. Implementiert hat man das dann als anonyme
        // innere Klasse.

        // Die Java-Entwickler haben sich da dann folgendes bei gedacht:
        // Ich will also fuer alle Elemente einer Liste etwas tun.
        // Dafuer hat man ja die ForEach-Schleife. Nun moechte man ja aber keine
        // Schleife schreiben, sondern einfach nur sagen
        // "Mach das fuer alle Elemente!"
        // Daher gibt es
        list.forEach(new Consumer<String>() {

            @Override
            public void accept(String t) {
                System.out.println(t);
            }
        });
        System.out.println("___________TRENNLINIE_____________");
        // Diese neue Methode, die intern einfach die
        // Funktion, die man ihr uebergibt fuer alle
        // Elemente ausfuehrt. Consumer ist das, was man anstelle von Closure
        // eingefuehrt hat.

        // Nun hat man angefangen, systematisch den Code wegzulassen, den der
        // Compiler aus dem Kontext schließen kann.
        // die Methode foreach will einen Consumer, daher kann "new Consumer"
        // weg.

        // list.forEach(<String>() {
        //
        // @Override
        // public void accept(String t) {
        // System.out.println(t);
        // }
        // };

        // Der naechste tolle Trick sind funktionale Interfaces. Da Consumer nur
        // eine Methode hat, kann der Compiler direkt diese Methode aufrufen.

        // list.forEach({
        //
        // String t {
        // System.out.println(t);
        // }
        // };

        // das es sich um Strings handelt, die verarbeitet werden sollen,
        // erkennt der Compiler an der Typisierung der Liste.
        // Um das ganze optisch abzurunden verwendet man "->".
        // Dann hat man:

        list.forEach(t -> System.out.println(t));
        System.out.println("___________TRENNLINIE_____________");

        // Diese Herleitung habe ich aus
        // "Mastering Lambdas: Java Programming in a Multicore World"
        // ISBN: 978-0-07-182962-5

    }

    public static void main(String[] args) {

        lambdas();
        parallel();
    }

    private static void parallel() {
        List<String> liste = Arrays.asList("hallo", "welt", "23", "42");
        liste.parallelStream().forEach(s -> System.out.println(s));
    }
}

