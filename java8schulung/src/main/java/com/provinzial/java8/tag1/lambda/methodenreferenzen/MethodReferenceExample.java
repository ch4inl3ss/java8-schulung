package com.provinzial.java8.tag1.lambda.methodenreferenzen;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class MethodReferenceExample {

    private static List<String> list = Arrays.asList("hallo", "welt", "23", "42");
    private static List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5);


    public static void gebeListeAus() {

        //Zuvor haben wir ja bereits gesehen, wie man mit einem Stream und einem Lambda
        //alle Elemente einer Liste ausgeben kann:

        list.forEach(s -> System.out.println(s));


        trennLinie();
        //Als naechstes gibt es einen Weg, das ganze noch kuerzer zu schreiben

        list.forEach(System.out::println);

        trennLinie();
        //Das ist eine sog. Methodenreferenz.
        //Methodenreferenzen verweisen direkt auf eine Methode.
        //Die Vorraussetzung ist, dass die Signatur passt.

        //Das funktioniert zum einen mit statischen Methoden:


        list.sort((String eins , String zwei )-> {
            return StringUtils.compare(eins, zwei);
        });
        list.forEach(System.out::println);


        trennLinie();
        //Aber auch mit einer einzelnen Referenz:

        String referenz = list.get(0);
        Supplier<Integer> laenge = referenz::length;
        System.out.println("laenge.get() = " + laenge.get());

        trennLinie();
        //Man kann sogar Konstruktoren als Methodenreferenz verwenden!

        List<TestClass> testClassList = intList.stream().map(TestClass::new).collect(Collectors.toList());
        testClassList.forEach(System.out::println);

    }

    public static void main(String[] args) {

        gebeListeAus();


    }

    private static void trennLinie() {
        System.out.println("___________________________________________________________");
        System.out.println();
    }




}
