package com.provinzial.java8.tag1.streams.aufgaben;

import java.util.Collections;
import java.util.List;

/**
 * @author PD05843
 */
public class Mapping {

    /**
     * Transformiert alle lowerCase Buchstaben zu UpperCase
     * @param lowerCase
     * @return Liste mit UpperCase Strings
     */
    public static List<String> toUpperCase(List<String> lowerCase) {
        // TODO Aufgabe 1a: Schreibe einen Algorithmus mit Streams, der eine Liste von Strings in eine Liste von
        // UPPERCASE-Strings transformiert. Führe danach den JUnit-Testfall aus, um das Ergebnis zu testen.
        return Collections.emptyList();
    }

    /**
     * Transformiert die Namensliste in eine Liste von Personal
     * @param namensListe
     * @return Liste mit Angestellten
     */
    public static List<Personal> toPersonal(List<String> namensListe) {
        // TODO Aufgabe 1b: Schreibe einen Algorithmus mit Streams, der eine Liste von Namen in eine Liste von
        // Personal-Objekten transformiert. Erweitere ggf. die Klasse Personal um eine Methoden-Referenz nutzen zu können.
        // Führe danach den JUnit-Testfall aus, um das Ergebnis zu testen.
        return Collections.emptyList();
    }

}
