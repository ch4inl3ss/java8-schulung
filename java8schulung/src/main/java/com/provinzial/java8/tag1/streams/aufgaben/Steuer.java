package com.provinzial.java8.tag1.streams.aufgaben;

public enum Steuer {
    MWST19(19),

    MWST7(7);

    private int prozentsatz;

    private Steuer(int prozentsatz) {
        this.prozentsatz = prozentsatz;
    }

    /**
     * @return the prozentsatz
     */
    public int getProzentsatz() {
        return prozentsatz;
    }

}
