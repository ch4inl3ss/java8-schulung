package com.provinzial.java8.tag1.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Streams {

    private static final String TEST_VNR = "20012345678";

    private static final Consumer<Void> TRENNER = (a) -> System.out.println("------------TRENNER------------");

    private static final Statistik FEST_STEHENDE_STATISTIK;

    static {
        FEST_STEHENDE_STATISTIK = new Statistik();
        FEST_STEHENDE_STATISTIK.setAnzahlGeschosse(2);
        FEST_STEHENDE_STATISTIK.setAnzahlGeschosse(2);
        FEST_STEHENDE_STATISTIK.setVnr(TEST_VNR);
        FEST_STEHENDE_STATISTIK.setPartnerIDs(Arrays.asList("12345678", "87654321"));
    }

    public static void main(String[] args) {
        List<Statistik> statistikList = createStatistikList(5);
        // Java 7

        // Java 8

    }

    private static List<Statistik> createStatistikList(int amount) {
        return IntStream.range(0, amount).boxed().map((i) -> createStatistik(i)).collect(Collectors.toList());
    }

    private static Statistik createStatistik(int i) {
        Statistik statistik = new Statistik();
        statistik.setVnr(String.valueOf(createRandomLong(10000000, 90000000 + i)));
        statistik.setAnzahlGeschosse(createRandomLong(1, 10));
        statistik.setAnzahlPersonen(createRandomLong(1, 10));
        statistik.setPartnerIDs(IntStream.range(0, 5).mapToObj(j -> createRandomLong(100, 900 + j)).map(String::valueOf)
            .collect(Collectors.toList()));
        return statistik;
    }

    private static long createRandomLong(long aStart, long aEnd) {
        Random random = new Random();
        // get the range, casting to long to avoid overflow problems
        long range = aEnd - aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * random.nextDouble());
        return (fraction + aStart);
    }

    private static class Statistik {
        private String vnr;

        private long anzahlPersonen;

        private long anzahlGeschosse;

        private List<String> partnerIDs;

        /**
         * @return the vnr
         */
        public String getVnr() {
            return vnr;
        }

        /**
         * @param vnr the vnr to set
         */
        public void setVnr(String vnr) {
            this.vnr = vnr;
        }

        /**
         * @return the anzahlPersonen
         */
        public long getAnzahlPersonen() {
            return anzahlPersonen;
        }

        /**
         * @param anzahlPersonen the anzahlPersonen to set
         */
        public void setAnzahlPersonen(long anzahlPersonen) {
            this.anzahlPersonen = anzahlPersonen;
        }

        /**
         * @return the anzahlGeschosse
         */
        public long getAnzahlGeschosse() {
            return anzahlGeschosse;
        }

        /**
         * @param anzahlGeschosse the anzahlGeschosse to set
         */
        public void setAnzahlGeschosse(long anzahlGeschosse) {
            this.anzahlGeschosse = anzahlGeschosse;
        }

        /**
         * @return the partnerIDs
         */
        public List<String> getPartnerIDs() {
            return partnerIDs;
        }

        /**
         * @param partnerIDs the partnerIDs to set
         */
        public void setPartnerIDs(List<String> partnerIDs) {
            this.partnerIDs = partnerIDs;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "Statistik [vnr=" + vnr + ", anzahlPersonen=" + anzahlPersonen + ", anzahlGeschosse=" + anzahlGeschosse
                + ", partnerIDs=" + partnerIDs + "]";
        }

    }

}
