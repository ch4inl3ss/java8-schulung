package com.provinzial.java8.examples.datetime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class JavaUtilDateProblem {

    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date(System.currentTimeMillis()+ 1000l*60*60*24*365);
        System.out.println("format.format(date) = " + format.format(date));
    }
}
