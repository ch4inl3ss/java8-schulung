package com.provinzial.java8.examples.money;

import java.util.Currency;
import java.util.Locale;

public class CurrencyExample {

    public static void main(String[] args) {
        Currency euro = Currency.getInstance(Locale.GERMANY);

        System.out.println("euro = " + euro);

        System.out.println("euro.getSymbol(Locale.GERMANY) = " + euro.getSymbol(Locale.GERMANY));
        
        Currency dollar = Currency.getInstance(Locale.US);

        System.out.println("dollar = " + dollar);

        System.out.println("dollar.getSymbol(Locale.US) = " + dollar.getSymbol(Locale.US));

    }
}
