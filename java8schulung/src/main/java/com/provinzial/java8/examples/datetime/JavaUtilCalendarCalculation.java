package com.provinzial.java8.examples.datetime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class JavaUtilCalendarCalculation {

    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        Calendar cal = new GregorianCalendar();

        cal.add(Calendar.DATE, 1);

        Date date = cal.getTime();
        System.out.println("heute = " + format.format(new Date()));
        System.out.println("morgen = " + format.format(date));
    }
}
