package com.provinzial.java8.examples;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ChainingPredicates {

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        Predicate<Integer> groesser5 = i-> i>5;
        Predicate<Integer> kleiner10 = i-> i<10;
        integerList.stream()
                .filter(groesser5.and(kleiner10))
                .forEach(System.out::println);
    }
}
