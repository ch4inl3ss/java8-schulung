package com.provinzial.java8.examples;

public class DefaultExample {

    interface A{
        default void foo(){
            System.out.println("Foo A");
        }
    }

    interface B{
        default void foo(){
            System.out.println("Foo B");
        }
    }

    static class DoubleImplement implements A, B{

        @Override
        public void foo() {
            A.super.foo();
        }
    }

    public static void main(String[] args) {
        new DoubleImplement().foo();
    }
}
