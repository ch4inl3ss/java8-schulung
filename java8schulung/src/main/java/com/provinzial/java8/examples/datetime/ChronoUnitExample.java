package com.provinzial.java8.examples.datetime;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class ChronoUnitExample {

    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate inAHundredDays = LocalDate.now().plusDays(100);

        System.out.println(Period.between(today, inAHundredDays)); //3 Months, 8 Days
        //um nur die Tage zu bekommen:

        System.out.println(ChronoUnit.DAYS.between(today, inAHundredDays)); // 100

    }
}
