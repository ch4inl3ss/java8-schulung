package com.provinzial.java8.examples.datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;

public class ZonedDateTimeExample {

    public static void main(String[] args) {
        ZoneId london = ZoneId.of("Europe/London");
        ZoneId berlin = ZoneId.of("Europe/Berlin");


        ZonedDateTime londonTime = ZonedDateTime.now(london);
        ZonedDateTime berlinTime = ZonedDateTime.now(berlin);

        System.out.println("londonTime = " + londonTime);
        System.out.println("berlinTime = " + berlinTime);

        System.out.println(berlinTime.getHour()-londonTime.getHour());

        System.out.println("londonTime.getOffset() = " + londonTime.getOffset());
        System.out.println("berlinTime.getOffset() = " + berlinTime.getOffset());
        System.out.println(berlinTime.getOffset().getTotalSeconds() /3600);
    }

}
