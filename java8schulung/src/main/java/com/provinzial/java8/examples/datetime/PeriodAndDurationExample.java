package com.provinzial.java8.examples.datetime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class PeriodAndDurationExample {

    public static void main(String[] args) {

        //use Period for Dates
        LocalDate today = LocalDate.now();
        LocalDate inTenDays = LocalDate.now().plusDays(10);

        System.out.println(Period.between(today, inTenDays).getDays()); //10

        //Use Duration for Time
        LocalTime now = LocalTime.now();
        LocalTime inTenSeconds = LocalTime.now().plusSeconds(10);

        System.out.println(Duration.between(now, inTenSeconds).getSeconds()); //10
    }
}
