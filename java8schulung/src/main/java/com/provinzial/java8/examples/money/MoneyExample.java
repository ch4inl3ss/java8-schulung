package com.provinzial.java8.examples.money;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

public class MoneyExample {

    public static void main(String[] args) {
        MonetaryAmount oneDollar = Monetary.getDefaultAmountFactory().setCurrency("EUR")
                .setNumber(1).create();

        MonetaryAmount amount = oneDollar.multiply(10);
        System.out.println("amount = " + amount);

        MonetaryAmount exception = oneDollar.divide(3);


        CurrencyConversion conversionEUR = MonetaryConversions.getConversion("CHF");

        MonetaryAmount convertedAmountUSDtoEUR = oneDollar.with(conversionEUR);
        System.out.println("convertedAmountUSDtoEUR = " + convertedAmountUSDtoEUR.toString());

    }
}
