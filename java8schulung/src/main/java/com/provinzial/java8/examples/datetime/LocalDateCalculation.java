package com.provinzial.java8.examples.datetime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateCalculation {

    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate heute = LocalDate.now();

        LocalDate morgen = heute
                .plusDays(1);

        System.out.println("heute = " + heute.format(formatter));
        System.out.println("morgen = " + morgen.format(formatter));
    }
}
