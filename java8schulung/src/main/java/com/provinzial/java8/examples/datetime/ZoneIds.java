package com.provinzial.java8.examples.datetime;

import java.time.ZoneId;

public class ZoneIds {
    public static void main(String[] args) {
        ZoneId.getAvailableZoneIds().stream().forEach(System.out::println);
        System.out.println("ZoneId.getAvailableZoneIds().size() = " + ZoneId.getAvailableZoneIds().size());
    }
}
