package com.provinzial.java8.examples.datetime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class JavaUtilDateCalculation {

    public static void main(String[] args) {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        Date heute = new Date();

        Date morgen = new Date(heute.getTime()+24*60*60*1000);

        System.out.println("heute = " + format.format(heute));
        System.out.println("morgen = " + format.format(morgen));

    }
}
