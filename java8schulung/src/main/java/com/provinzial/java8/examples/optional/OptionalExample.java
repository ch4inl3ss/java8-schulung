package com.provinzial.java8.examples.optional;

import java.util.Optional;

public class OptionalExample {

    public static void main(String[] args) {

        //quasi null
        Optional<String> optionalEmpty = Optional.empty();
        Optional<String> optionalFull = Optional.of("HALLO");
        System.out.println("optionalEmpty = " + optionalEmpty.isPresent()); //false

        //mit Optional.of ein nicht-nullable Wert wrappen
        Optional.of( "Hallo" ); //Optional.isPresent() = true
        //Optional.of(null); //NullpointerException
        Optional.ofNullable(null); //Optional.isPresent() = false

        //mit Optional.get() bekommt man den Wert des Optionals, aber man kann einen Nullpointer bekommen
        //Optional.ofNullable(null).get(); //NullpointerException
        Optional.of("Hallo").get(); // "Hallo"

        //mit .orElse kann man fuer den null-Fall einen Default angeben:
        optionalEmpty.orElse("default").length(); //7 (Laenge von "default")

        //mit .orElseThrow kann man eine spezifische Exception schmeissen:
        optionalEmpty.orElseThrow(IllegalStateException::new);

        //mit .isPresent kann man pruefen, ob das Optional tatsaechlich einen Wert hat
        optionalEmpty.isPresent(); // false (weil empty)

        //das wirklich coole ist die Moeglichkeit von .ifPresent()
        //Mit .ifPresent() kann man in einem Lambda eine Funktion aufrufen:
        optionalFull.ifPresent(System.out::println); // "HALLO"

    }
}
