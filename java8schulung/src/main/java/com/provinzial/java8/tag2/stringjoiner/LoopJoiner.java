package com.provinzial.java8.tag2.stringjoiner;

import java.util.Arrays;
import java.util.List;

public class LoopJoiner {

    private List<String> names = Arrays.asList("Dade Murphy", "Kate Libby", "Joey Pardella", "Emmanuel Goldstein", "Paul Cook");

    public String joinString() {
        //TODO Aufgabe 21: Hier sollen die Namen aus names in folgendem Format ausgegeben werden:
        // Namen: [ Name: Dade Murphy, Name: Kate Libby, ... ]
        // Nutze dazu einen StringBuilder und eine Schleife.
        return null;
    }
}
