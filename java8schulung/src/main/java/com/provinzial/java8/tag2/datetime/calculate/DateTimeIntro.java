package com.provinzial.java8.tag2.datetime.calculate;



/**
 * Created by ch4inl3ss on 07.06.17.
 */
public class DateTimeIntro implements  DateHandler{

    //TODO Aufgabe 12: Die folgendenden vier Methoden sollen mit java.time.LocalDate implementiert werden

    //Verwende fuer die Implementierung
    //    java.time.LocalDate
    //und fuer die Formatierung
    //    java.time.format.DateTimeFormatter

    @Override
    public String getToday() { return null;    }

    @Override
    public String getTommorow() {
        return null;
    }

    @Override
    public String getNextWeek() {
        return null;
    }

    @Override
    public String getNextYear() {
        return null;
    }
}
