package com.provinzial.java8.tag2.optional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class CustomerFinderWithOptional {


    public Stream<Customer> findCustomers(Collection<String> customerIds) {
        //TODO Aufgabe 20: hier muss fuer jede ID in customerIDs findCustomer aufgerufen werden.
        //Dann hat man ein Stream von Optional<Customer>. Dann muss man den filtern
        //und am ende auf den echten Wert von optional mappen.
        // Das geht mit einem Ausdruck.
        return null;
    }




    private Optional<Customer> findCustomer(String id){
        return Optional.ofNullable(customers.get(id));
    }


    public static final Map<String, Customer> customers = new HashMap<String, Customer>(){
        {
            put("1", new Customer("1", "Otto", 22));
            put("2", new Customer("2", "Dirk", 33));
            put("3", null);
            put("4", new Customer("4", "Henri", 21));
            put("5", null);
        }
    };

}
