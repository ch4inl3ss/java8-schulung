package com.provinzial.java8.tag2.strategypattern;

import com.provinzial.java8.tag2.optional.Customer;

public interface PrintStrategy {

    String print(Customer customer);
}
