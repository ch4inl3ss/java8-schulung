package com.provinzial.java8.tag2.decoratorpattern;

import com.provinzial.java8.tag2.decoratorpattern.old.BasicText;
import com.provinzial.java8.tag2.decoratorpattern.old.PigDecorator;
import com.provinzial.java8.tag2.decoratorpattern.old.RoseDecorator;

public class ContextClass {


    public static void main(String[] args) {
        System.out.println(new PigDecorator(new RoseDecorator(new BasicText(" HALLO! "))).gibText());
    }
}
