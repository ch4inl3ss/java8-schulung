package com.provinzial.java8.tag2.strategypattern.old;

import com.provinzial.java8.tag2.optional.Customer;
import com.provinzial.java8.tag2.strategypattern.PrintStrategy;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class JSONPrintStrategy implements PrintStrategy {

    @Override
    public String print(Customer customer) {
        return ReflectionToStringBuilder.toString(customer, ToStringStyle.JSON_STYLE);
    }
}
