package com.provinzial.java8.tag2.decoratorpattern.old;

import java.time.format.DateTimeFormatter;

public class YourDecorator {

    //TODO Aufgabe 25: Aus dieser Klasse eine valide Implemetierung eines Decorators machen.
    //Siehe z.B. Rose-Decorator oder PigDecorator.
    //der Text, den dieser Decorator anfuegen soll, ist in der Konstante "TEXT".

    private static final String TEXT = "funktioniert";

}
