package com.provinzial.java8.tag2.strategypattern.functional;

import com.provinzial.java8.tag2.optional.Customer;
import com.provinzial.java8.tag2.strategypattern.PrintStrategy;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.stream.Stream;

public class FunctionalStrategy {

    //https://www.youtube.com/watch?v=e4MT_OguDKg


    public static PrintStrategy liefereJsonPrintStrategy(){
        //TODO Aufgabe 24 a: implementiere hier die JSON-Printstrategy als ein Lambda-Ausdruck

        return null;
    }

    public static PrintStrategy liefereShortPrefixPrintStrategy(){
        //TODO Aufgabe 24 b: implementiere hier die Short-Prefix-Printstrategy als Lambda-Ausdruck

        return null;
    }


    public static void main(String[] args) {


        //Hier ein Tipp - So konnte man das in Java 7 mit einer anonymen inneren Klasse implementieren:
        PrintStrategy json = new PrintStrategy() {
            @Override
            public String print(Customer customer) {
                return ReflectionToStringBuilder.toString(customer, ToStringStyle.JSON_STYLE);
            }
        };

        Customer phil = new Customer("10", "Phil", 40);

        System.out.println("jsonStrategy.print(phil) = " + json.print(phil));

        //Das hier funktioniert erst, wenn die Methoden beide implementiert sind.
        //Ob die Methoden richtig implementiert sind, sagt der Testfall.
        System.out.println("jsonStrategy.print(phil) = " + liefereJsonPrintStrategy().print(phil));
        System.out.println("shortPrefixStrategy.print(phil) = " + liefereShortPrefixPrintStrategy().print(phil));

    }
}
