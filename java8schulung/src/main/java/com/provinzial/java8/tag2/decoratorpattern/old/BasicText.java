package com.provinzial.java8.tag2.decoratorpattern.old;

public class BasicText implements Text {

    private String text;

    public BasicText(String text){
        this.text=text;

    }

    @Override
    public String gibText() {
        return text;
    }
}
