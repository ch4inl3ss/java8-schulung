package com.provinzial.java8.tag2.decoratorpattern.functional;

import java.util.function.Function;

public class FunctionChainingDecorator {


    public static Function<String, String> roseFunction(){
        return s-> s + "--------{---(@ ";
    }

    public static Function<String, String> pigFunction(){
        return s-> s +"^(*(oo)*)^ ";
    }

    //TODO: Aufgabe 26 a: Passe die Methode yourFunction so an, dass sie wie eine der anderen functions funktioniert.
    //Das Ziel ist, dass diese Methode genau so aufgerufen werden kann wie
    //die anderen Methoden roseFunction() und pigFunction() und einen String
    // "YourFunction" anfuegt.

    public static Function<String, String> yourFunction(){
        return null;
    };

    //TODO: Aufgabe 26 b: Verwende yourFunction als Decorator indem du andThen auf der ausgangsFunktion verwendest.
    // apply die ausgangsFunktion auf den String "funtkioniert".
    public static String chained(Function<String, String>ausgangsFunktion){
       return null;
    }



    public static Function<String,String> basicFunction(){
        return s->s;
    }


    //Macht das gleiche wie vorher ContextClass
    public static void main(String[] args) {
        String result=basicFunction().andThen(roseFunction()).andThen(pigFunction()).apply("HALLO!  ");

        System.out.println("result = " + result);
    }

}
