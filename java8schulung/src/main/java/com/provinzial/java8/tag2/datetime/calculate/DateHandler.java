package com.provinzial.java8.tag2.datetime.calculate;

/**
 * Created by ch4inl3ss on 26.06.17.
 */
public interface DateHandler {


    String getToday();

    String getTommorow();

    String getNextWeek();

    String getNextYear();


}
