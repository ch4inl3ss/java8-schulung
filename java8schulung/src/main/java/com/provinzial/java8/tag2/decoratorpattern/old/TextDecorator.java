package com.provinzial.java8.tag2.decoratorpattern.old;

public class TextDecorator implements Text {


    protected Text text;

    public TextDecorator(Text text){
        this.text=text;
    }


    @Override
    public String gibText() {
        return this.text.gibText();
    }


}
