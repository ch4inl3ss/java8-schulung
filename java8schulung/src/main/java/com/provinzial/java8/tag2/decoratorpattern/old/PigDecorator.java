package com.provinzial.java8.tag2.decoratorpattern.old;


public class PigDecorator extends TextDecorator {

    private static final String PIG="^(*(oo)*)^";


    public PigDecorator(Text text) {
        super(text);
    }

    @Override
    public String gibText() {
        return super.gibText()+ " "+ PIG;
    }
}
