package com.provinzial.java8.tag2.decoratorpattern.old;

public class RoseDecorator extends TextDecorator{

    private static final String ROSE= "--------{---(@";


    public RoseDecorator(Text text) {
        super(text);
    }

    @Override
    public String gibText() {
       return super.gibText()+ " "+ ROSE;
    }
}
