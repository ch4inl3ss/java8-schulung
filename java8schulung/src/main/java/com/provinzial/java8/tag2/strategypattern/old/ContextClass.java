package com.provinzial.java8.tag2.strategypattern.old;

import com.provinzial.java8.tag2.optional.Customer;
import com.provinzial.java8.tag2.strategypattern.PrintStrategy;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ContextClass {

    public static void main(String[] args) {
        PrintStrategy printStrategy = new JSONPrintStrategy();

        Customer phil = new Customer("10", "Phil", 40);

        String print = printStrategy.print(phil);
        System.out.println("print = " + print);


    }
}
