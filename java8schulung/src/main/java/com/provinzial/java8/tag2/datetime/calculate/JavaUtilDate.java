package com.provinzial.java8.tag2.datetime.calculate;

/**
 * Created by ch4inl3ss on 26.06.17.
 */
public class JavaUtilDate implements  DateHandler{

    //TODO Aufgabe 10: Die folgendenden vier Methoden sollen ausschliesslich mit java.util.Date implementiert werden.
    //Fuer diese Implementierung verwende
    //    java.util.Date
    //und fuer die Formtatierung
    //    java.text.SimpleDateFormat

    public String getToday() {
        return null;
    }

    public String getTommorow() {
        return null;
    }

    public String getNextWeek() {
        return null;
    }

    public String getNextYear() {
        return null;
    }
}
