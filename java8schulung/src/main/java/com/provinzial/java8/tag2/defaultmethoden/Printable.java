package com.provinzial.java8.tag2.defaultmethoden;


import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public interface Printable {

    //TODO aendere die Methode print() so, dass sie es einem ermoeglicht, sie auf dem Interface aufzurufen.
    //Lass die Methode einfach per ReflectionToStringBuilder this zurueckgeben.
    //Verwende ausserdem dem ToStringStyle.JSON_STYLE

      String print();


}
