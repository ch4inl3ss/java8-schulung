# Vorbereitung für die Java-8 Schulung (Update 27.07.2017)
## Themen an Tag 1:
### Einleitung Lambda (Felix)
### Methoden Referenzen (Felix)
### Lambda Praxis + Stream (Bastian)

## Themen an Tag 2:
### Fragen zu Tag 1 
### Funktionale Programmierung (Sebastian)
### Datetime/Currency (Felix)
### Ausblick Java 9 (Sebastian)

## Anwesenheit

Probe:
1 - Felix Bastian
2 - Felix

Schulung:
1 - Bastian, Felix
2 - Sebastian, Felix




# Vorbereitung für die Java-8 Schulung
## Themen an Tag 1:
### Datetime-Api
### Currency-Api
### Einführung in Lambda-Ausdrücke

## Themen an Tag 2
### Praktische Übungen mit Lambdaausdrücken
### Map, Filter
### Collectors

## Themen an Tag 3
### Einführung in die funktionale Programmierung
### Methodenreferenzen
### Ausblick auf Java 9

# Vorbereitung für die Java-8 Schulung
## Themen an Tag 1:
### Datetime-Api
### Currency-Api
### Einführung in Lambda-Ausdrücke

## Themen an Tag 2
### Praktische Übungen mit Lambdaausdrücken
### Map, Filter
### Collectors

## Themen an Tag 3
### Einführung in die funktionale Programmierung
### Methodenreferenzen
### Ausblick auf Java 9
